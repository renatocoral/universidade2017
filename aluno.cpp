#include "aluno.hpp"
#include <iostream> 

using namespace std;

Aluno::Aluno(){
   setNome("");
   setMatricula("");
   setIdade(0);
   setSexo("");
   setTelefone("");
   setIra(5.0);
   setSemestre(1);
   setCurso("Engenharias");
}

Aluno::Aluno(string nome, string matricula, string telefone, int idade, string sexo){
   setNome(nome);
   setMatricula(matricula);
   setIdade(idade);
   setSexo(sexo);
   setTelefone(telefone);
   setIra(5.0);
   setSemestre(1);
   setCurso("Engenharias");
}
Aluno::~Aluno(){}

void Aluno::setIra(float ira){
   this->ira = ira;
}
float Aluno::getIra(){
   return ira;
}
void Aluno::setSemestre(int semestre){
   this->semestre = semestre;
}
int Aluno::getSemestre(){
   return semestre;
}
void Aluno::setCurso(string curso){
   this->curso = curso;
}
string Aluno::getCurso(){
   return curso;
}

void Aluno::imprimeDadosAluno(){
   cout << getNome() << "\t";
   cout << getMatricula() << "\t";
   cout << getTelefone() << "\t";
   cout << getSexo() << "\t";
   cout << getIdade() << "\t";
   cout << getIra() << "\t";
   cout << getSemestre() << "\t";
   cout << getCurso() << endl;
}






