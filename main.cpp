#include <iostream>
#include "pessoa.hpp"
#include "aluno.hpp"
#include <sstream>

using namespace std;

int main(int argc, char ** argv) {

   // Criação de objetos com alocação estática
   Pessoa pessoa_1;
   Pessoa pessoa_2("Maria", "555-1111", 21);

   pessoa_1.setNome("Joao");
   pessoa_1.setMatricula("14/0078070");
   pessoa_1.setTelefone("555-5555");
   pessoa_1.setSexo("M");
   pessoa_1.setIdade(20);

   // Criação de objetos com alocação dinâmica
   Pessoa * pessoa_3;
   pessoa_3 = new Pessoa();
   Pessoa * pessoa_4;
   pessoa_4 = new Pessoa("Marcelo", "666-7777", 25);   
  
   pessoa_3->setNome("Pateta");
   pessoa_3->setMatricula("10/12312313");
   pessoa_3->setTelefone("4444-1111");
   pessoa_3->setSexo("M");
   pessoa_3->setIdade(12);
   
   // Impressão dos atributos dos objetos utilizando método da própria classe
   pessoa_1.imprimeDados();
   pessoa_2.imprimeDados();
   pessoa_3->imprimeDados();
   pessoa_4->imprimeDados();

   // Liberação de memória dos objetos criados dinamicamente
   delete(pessoa_3);
   delete(pessoa_4);

   // Criação de objeto da classe filha
   Aluno aluno_1;
   aluno_1.setNome("João");
   cout << "Curso do aluno " << aluno_1.getNome() << ": " << aluno_1.getCurso() << endl;

   cout << endl << endl << endl;
   // Exemplo de cadastro de uma lista de Alunos
   Aluno *lista_de_alunos[20]; // Criação da lista de tamanho fixo de ponteiros do objeto Aluno 

   string nome;
   string matricula;
   int idade;
   string sexo;
   string telefone;
   float ira;
   int semestre;
   string curso;

   // Entrada de dados do terminal (stdin)
   int quantidade = 0;
   int cadastra = 1;
   string input;
   while(cadastra) {
      cout << "Adiciona novo item? (1 = Sim | 2 = Não)" << endl;
      getline(cin, input);
      stringstream input_stream(input);
      input_stream >> cadastra;
      if (cadastra == 1) {
         cout << "Nome: ";
         getline (cin,nome);
         cout << "Matricula: ";
         getline(cin, matricula);
         cout << "Idade:";
         getline(cin, input);
         stringstream input_idade(input);
         input_idade >> idade;
         cout << "Telefone:";
         getline(cin, telefone);
         cout << "Sexo:";
         getline(cin, sexo);
         lista_de_alunos[quantidade++] = new Aluno(nome, matricula,  telefone, idade, sexo);
      }
      else
         break;
   }

   // Imprime lista de Alunos
   cout << "Nome \t Matricula \t Telefone \t Sexo \t Idade \t Ira \t Semestre \t Curso" << endl;
   for (int i = 0; i < quantidade; ++i) {
      lista_de_alunos[i]->imprimeDadosAluno();
   }

   return 0;
}
